angular.module("app", ['angular-meteor', 'ngMaterial'])

  .config(['$mdThemingProvider', '$interpolateProvider', function($mdThemingProvider, $interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
    
    $mdThemingProvider.definePalette('true-grey', {
    '50': 'FAFAFA',
    '100': 'F5F5F5',
    '200': 'EEEEEE',
    '300': 'E0E0E0',
    '400': 'BDBDBD',
    '500': '9E9E9E',
    '600': '757575',
    '700': '616161',
    '800': '424242',
    '900': '212121',
    'A100': 'F5F5F5',
    'A200': 'EEEEEE',
    'A400': 'BDBDBD',
    'A700': '616161',
    'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
                                        // on this palette should be dark or light
    'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
     '200', '300', '400', 'A100'],
    'contrastLightColors': undefined    // could also specify this if default was 'dark'
  });
    
    
    $mdThemingProvider.theme('default')
      .primaryPalette('true-grey')
      .accentPalette('light-green')
  }]);
  
