angular.module('app').controller('body-controller', ['$window', '$mdMedia', '$mdSidenav', '$timeout', 
function($window, $mdMedia, $mdSidenav, $timeout) {
  var vm = this;
  
  vm.links = links();
  vm.clickMenu = clickMenu;
  vm.clickLink = clickLink;
  
  function clickLink(path) {
    $mdSidenav('right').close().then(animate_sidenav);
    Router.go("/"+path);
  }
  
  function clickMenu() {
    $mdSidenav('right').toggle().then(animate_sidenav);
  }
  
  function animate_sidenav(){
    //angular.element('#main-sidenav').addClass('md-closed-add md-closed-add-active')
  }
  
  function links() {
    return [
      {
        name: "Thoughts",
        path: 'thoughts'
      },
      {
        name: "Projects",
        path: 'projects'
      },
      {
        name: "Research",
        path: 'research'
      },
      {
        name: "Contact",
        path: 'contact-me'
      }
    ]
  }
  
  reactive_height();
  angular.element($window).on('resize', angular.bind(vm, function(){
    reactive_height();
  }))
  function reactive_height() {
    angular.element('#body').height($window.innerHeight);
  }
  
}])