angular.module('app').directive('toolbarItem', toolbarItemDirective);
angular.module('app').directive('toolbarMenu', toolbarMenuDirective);

function toolbarItemDirective() {
  return {
    restrict: 'E',
    transclude: true,
    template: getTemplate
  }
  
  function getTemplate(element, attr) {
    return '<md-button class="toolbar-button" href="/'+attr.path+'">'+attr.name+'</md-button>'
  }
}

function toolbarMenuDirective() {
  return {
    restrict: 'E',
    transclude: true,
    template: getTemplate
  }
  
  function getTemplate(element, attr) {
    return '<md-button ng-click="body.clickMenu()" aria-label="Side Navigation"><core-icon icon="'+attr.icon+'"></core-icon></md-button>'
  }
}