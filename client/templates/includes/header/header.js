angular.module('app').controller('header-controller', ['$mdMedia', function($mdMedia){
  var vm = this
  vm.isScreenSmall = function() { return $mdMedia('min-width: 850px'); }
}])

Template.header.helpers({
  links: function() {
    return [
      {
        name: "Thoughts",
        path: 'thoughts'
      },
      {
        name: "Projects",
        path: 'projects'
      },
      {
        name: "Research",
        path: 'research'
      },
      {
        name: "Contact",
        path: 'contact-me'
      }
    ]
  }
})

// Template.postsList.rendered = function () {
//   this.find('#main-sidenav')._uihooks = {
//     enterElement: function (node, next) {
//       // do nothing for now
//     }
//   }
// }
