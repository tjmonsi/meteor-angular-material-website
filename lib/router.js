// Global Configuration
Router.configure({
  layoutTemplate: 'layout',
  notFoundTemplate: 'notFound'
});

// Routes
Router.route('/', { name: 'mainPage'})
Router.route('/thoughts', function(){
  this.render('thoughtsPage')
})

// Route Configuration

// Private functions
